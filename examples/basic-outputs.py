from ccmicro import ccmicro
import os

if "PHOTOSET_UNC" in os.environ:
	ccmicro.buildProducts(os.environ.get("PHOTOSET_UNC"))
else:
	ccmicro.buildProducts("C:\\data\\ara_2d_grid_3m_photos_tinyset")