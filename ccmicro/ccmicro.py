#!/usr/bin/env python
# ******************************************************************************
# Minimal set of calls to generate context capture output for STAPLS+
# ******************************************************************************

import sys
import os
import shutil
import time
import ccmasterkernel
from glob import glob
import zipfile


class Unbuffered(object):
    def __init__(self, stream):
        self.stream = stream

    def write(self, data):
        self.stream.write(data)
        self.stream.flush()

    def writelines(self, datas):
        self.stream.writelines(datas)
        self.stream.flush()

    def __getattr__(self, attr):
        return getattr(self.stream, attr)


def zipdir(path, ziph):
    # ziph is zipfile handle
    for root, dirs, files in os.walk(path):
        for file in files:
            ziph.write(os.path.join(root, file))


def buildObjProduction(project, reconstruction):
    """
    Build production based on existing reconstruction.
    Returns production obj ready for attach
    """
    production_obj = ccmasterkernel.Production(reconstruction)
    production_obj.addProductionJob(ccmasterkernel.TileProductionJob(
        production_obj, reconstruction.getInternalTile(0)))
    production_obj.sortJobsAccordingToDistanceToCenter()
    reconstruction.addProduction(production_obj)

    production_obj.setDriverName('OBJ')
    production_obj.setDestination(os.path.join(
        project.getProductionsDirPath(), production_obj.getName()))

    driverOptions = production_obj.getDriverOptions()
    driverOptions.put_bool('TextureEnabled', True)
    driverOptions.put_int('TextureCompressionQuality', 80)
    driverOptions.writeXML(os.path.join(
        project.getProductionsDirPath(), "options.xml"))

    production_obj.setDriverOptions(driverOptions)

    return production_obj


def buildLasProduction(project, reconstruction):
    """
    Build production based on existing reconstruction.
    Returns production obj ready for attach
    """

    lasOptions = {
        'SRS': "string",         # Default: ""
        # {'pixel','meter','unit'} Default: 'pixel'
        'PointSamplingUnit': "string",
        'PointSamplingDistance': "double",         # Default: 1
        'CompressionMethod': "string",         # {'None','LAZ'} Default: 'None'
        'TextEnabled': "bool",           # {true, false} Default: true
        # {'Visible', 'Thermal'} Default: 'Visible'
        'TextureColorSource': "string"
    }

    production_las = ccmasterkernel.Production(reconstruction)
    production_las.addProductionJob(ccmasterkernel.TileProductionJob(
        production_las, reconstruction.getInternalTile(0)))
    production_las.sortJobsAccordingToDistanceToCenter()
    reconstruction.addProduction(production_las)

    production_las.setDriverName('LAS')
    production_las.setDestination(os.path.join(
        project.getProductionsDirPath(), production_las.getName()))

    driverOptions = production_las.getDriverOptions()

    driverOptions.writeXML(os.path.join(
        project.getProductionsDirPath(), "options.xml"))

    production_las.setDriverOptions(driverOptions)

    return production_las


def launchAndWaitForProduction(production):
    """
    Returns tuple (SuccessBoolean, outputDir)
    """
    productionSubmitError = production.submitProcessing()

    if not productionSubmitError.isNone():
        print('Error: Failed to submit production.')
        print(productionSubmitError.message)
        sys.exit(0)

    print('The production job submitted,waiting to be processed...')

    iPreviousProgress = 0
    iProgress = 0
    previousJobStatus = ccmasterkernel.JobStatus.Job_unknown

    while 1:
        jobStatus = production.getProductionJob(0).getJobStatus()

        if jobStatus != previousJobStatus:
            print(ccmasterkernel.jobStatusAsString(jobStatus))

        if jobStatus == ccmasterkernel.JobStatus.Job_failed or jobStatus == ccmasterkernel.JobStatus.Job_cancelled or jobStatus == ccmasterkernel.JobStatus.Job_completed:
            break

        if iProgress != iPreviousProgress:
            print('%s%% - %s' %
                  (iProgress, production.getProductionJob(0).getJobMessage()))

        iPreviousProgress = iProgress
        iProgress = production.getProductionJob(0).getJobProgress()
        time.sleep(1)
        production.updateStatus()
        previousJobStatus = jobStatus

    print('')

    if jobStatus != ccmasterkernel.JobStatus.Job_completed:
        print('"Error: Incomplete production.')

        if production.getProductionJob(0).getJobMessage() != '':
            print(production.getProductionJob(0).getJobMessage())
        return(False, None)
    # --------------------------------------------------------------------
    # Report
    # --------------------------------------------------------------------
    print('Production completed.')
    outputDir = production.getDestination()
    print('Output directory: %s' % outputDir)

    return(True, outputDir)


def buildProducts(photoSetUNC, projectDir=None, outputDir=None):
    """

    """
    # Presume photos are in first directory containing a jpg
    photosDirPath = photoSetUNC

    if os.path.exists("project"):
        shutil.rmtree("project")
    os.makedirs("project")
    # Note: replace paths with your references, or install SDK sample data in C:/CC_SDK_DATA
    projectDirPath = os.path.join(os.getcwd(), "project")

    sys.stdout = Unbuffered(sys.stdout)

    print('MasterKernel version %s' % ccmasterkernel.version())
    print('')

    if not ccmasterkernel.isLicenseValid():
        print("License error: ", ccmasterkernel.lastLicenseErrorMsg())
        sys.exit(0)

    # --------------------------------------------------------------------
    # create project
    # --------------------------------------------------------------------
    projectName = os.path.basename(projectDirPath)

    project = ccmasterkernel.Project()
    project.setName(projectName)
    project.setDescription('Automatically generated from python script')
    project.setProjectFilePath(os.path.join(projectDirPath, projectName))
    err = project.writeToFile()
    if not err.isNone():
        print(err.message)
        sys.exit(0)

    print('Project %s successfully created.' % projectName)
    print('')

    # --------------------------------------------------------------------
    # create block
    # --------------------------------------------------------------------
    block = ccmasterkernel.Block(project)
    project.addBlock(block)

    block.setName('block #1')
    block.setDescription('input block')
    block.setPhotoDownsamplingRate(1.0)
    photogroups = block.getPhotogroups()
    files = os.listdir(photosDirPath)

    for file in files:
        file = os.path.join(photosDirPath, file)

        # add photo, create a new photogroup if needed
        lastPhoto = photogroups.addPhotoInAutoMode(file)

        if lastPhoto is None:
            print('Could not add photo %s.' % file)
            continue

        # upgrade block positioningLevel if a photo with position is found (GPS tag)
        if not lastPhoto.pose.center is None:
            block.setPositioningLevel(
                ccmasterkernel.PositioningLevel.PositioningLevel_georeferenced)

    print('')

    # check block
    print('%s photo(s) added in %s photogroup(s):' %
          (photogroups.getNumPhotos(), photogroups.getNumPhotogroups()))

    photogroups = project.getBlock(0).getPhotogroups()

    for i_pg in range(0, photogroups.getNumPhotogroups()):
        print('photogroup #%s:' % (i_pg+1))
        if not photogroups.getPhotogroup(i_pg).hasValidFocalLengthData():
            print('Warning: invalid photogroup')
        for photo_i in photogroups.getPhotogroup(i_pg).getPhotoArray():
            print('image: %s' % photo_i.imageFilePath)

        print('')

    if not block.isReadyForAT():
        if block.reachedLicenseLimit():
            print('Error: Block size exceeds license capabilities.')
        if block.getPhotogroups().getNumPhotos() < 3:
            print('Error: Insufficient number of photos.')
        else:
            print('Error: Missing focal lengths and sensor sizes.')
        sys.exit(0)

    # --------------------------------------------------------------------
    # AT
    # --------------------------------------------------------------------
    blockAT = ccmasterkernel.Block(project)
    project.addBlock(blockAT)
    blockAT.setBlockTemplate(
        ccmasterkernel.BlockTemplate.Template_adjusted, block)
    blockAT.setPositioningLevel(
        ccmasterkernel.PositioningLevel.PositioningLevel_georeferenced)
    err = project.writeToFile()
    if not err.isNone():
        print(err.message)
        sys.exit(0)

    atSubmitError = blockAT.getAT().submitProcessing()

    if not atSubmitError.isNone():
        print('Error: Failed to submit aerotriangulation.')
        print(atSubmitError.message)
        sys.exit(0)

    print('The aerotriangulation job has been submitted and is waiting to be processed...')

    iPreviousProgress = 0
    iProgress = 0
    previousJobStatus = ccmasterkernel.JobStatus.Job_unknown
    jobStatus = ccmasterkernel.JobStatus.Job_unknown

    while 1:
        jobStatus = blockAT.getAT().getJobStatus()

        if jobStatus != previousJobStatus:
            print(ccmasterkernel.jobStatusAsString(jobStatus))

        if jobStatus == ccmasterkernel.JobStatus.Job_failed or jobStatus == ccmasterkernel.JobStatus.Job_cancelled or jobStatus == ccmasterkernel.JobStatus.Job_completed:
            break

        if iProgress != iPreviousProgress:
            print('%s%% - %s' % (iProgress, blockAT.getAT().getJobMessage()))

        iPreviousProgress = iProgress
        iProgress = blockAT.getAT().getJobProgress()
        time.sleep(1)
        blockAT.getAT().updateJobStatus()

        previousJobStatus = jobStatus

    if jobStatus != ccmasterkernel.JobStatus.Job_completed:
        print('"Error: Incomplete aerotriangulation.')

        if blockAT.getAT().getJobMessage() != '':
            print(blockAT.getAT().getJobMessage())

    print('Aerotriangulation completed.')

    if not blockAT.isReadyForReconstruction():
        print('Error: Incomplete photos. Cannot create reconstruction.')
        sys.exit(0)

    blockExportOptions = ccmasterkernel.BlockExportOptions()
    blockExportOptions.jpegQuality = 80
    blockExportOptions.compressXMLFile = False
    blockExportOptions.rotationFormat = ccmasterkernel.bindings.RotationFormat.RotationFormat_matrix
    blockExportOptions.srs = ""

    # Export the xml file
    blockAT.exportToBlocksExchangeXML(
        projectDirPath + "/at.xml", blockExportOptions)

    print('Ready for reconstruction.')

    if blockAT.getPhotogroups().getNumPhotosWithCompletePose_byComponent(1) < blockAT.getPhotogroups().getNumPhotos():
        print('Warning: incomplete photos. %s/%s photo(s) cannot be used for reconstruction.' % (blockAT.getPhotogroups().getNumPhotos() -
                                                                                                 blockAT.getPhotogroups().getNumPhotosWithCompletePose_byComponent(1), blockAT.getPhotogroups().getNumPhotos()))

    # --------------------------------------------------------------------
    # Reconstruction
    # --------------------------------------------------------------------
    reconstruction = ccmasterkernel.Reconstruction(blockAT)
    blockAT.addReconstruction(reconstruction)
    tiling = ccmasterkernel.Tiling()
    tiling.targetMemoryUse = 0.5  # Use quarter of master systems memory (8GB)
    tiling.setTilingMode = ccmasterkernel.TilingMode.TilingMode_regularPlanarGrid
    reconstruction.setTiling(tiling)

    if reconstruction.getNumInternalTiles() == 0:
        print('Error: Failed to create reconstruction layout.')
        sys.exit(0)

    print('Reconstruction item created.')

    # --------------------------------------------------------------------
    # Production
    # --------------------------------------------------------------------
    production_obj = buildObjProduction(project, reconstruction)
    print('Production item created.')

    base = os.getcwd()

    status, outputDir = launchAndWaitForProduction(production_obj)

    os.chdir(outputDir)

    zipf = zipfile.ZipFile('../Production_OBJ.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('./', zipf)
    zipf.close()

    os.chdir(base)

    production_las = buildLasProduction(project, reconstruction)
    print('Production item created.')

    status, outputDir = launchAndWaitForProduction(production_las)

    os.chdir(outputDir)

    zipf = zipfile.ZipFile('../Production_LAS.zip', 'w', zipfile.ZIP_DEFLATED)
    zipdir('./', zipf)
    zipf.close()


if __name__ == '__main__':
    buildOutputs(os.environ.get('PHOTOSET_UNC'))
