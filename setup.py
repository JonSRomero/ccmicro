import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

install_requires=[
   'ccmasterkernel'
]

setuptools.setup(
    name="ccmicro",
    version="0.0.1",
    author="Kevin Clarke",
    author_email="kclarke@ara.com",
    description="Simplified Context Capture wrapper interface",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://www.ara.com",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: Other/Proprietary License",
        "Operating System :: Microsoft :: Windows :: 10",
    ],
)